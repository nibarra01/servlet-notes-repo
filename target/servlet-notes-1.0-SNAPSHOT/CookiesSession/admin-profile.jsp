<%--
  Created by IntelliJ IDEA.
  User: student114
  Date: 10/22/20
  Time: 8:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Session & Cookies Profile</title>
    <jsp:include page="/partials/bootstrap.jsp" />
</head>
<body>
<%@ include file="/partials/navbar.jsp"%>
<%@ include file="header.jsp"%>


<div class="container">
    <div class="row text-center">
        <div class="col-12">
            <span class="font-size-32 font-aldrich"> Viewing Profile </span>
        </div>
        <div class="col-12">
            <span class="font-size-24 font-aldrich"> Logged in successfully </span>
        </div>
        <div class="col-12">
            <span class="font-size-18 font-aldrich"> Hello, ${username} <br>Have you considered the security risks of using "password123" as your password?</span>
        </div>
        <div class="col-12 font-aldrich">
            <a href="/admin-logout">Logout</a>
        </div>
    </div>
</div>

<%@ include file="footer.jsp"%>

</body>
</html>

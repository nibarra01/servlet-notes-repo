<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student114
  Date: 10/20/20
  Time: 10:28 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>index</title>
    <jsp:include page="/partials/bootstrap.jsp" />
    <link rel="stylesheet" href="/partials/googlefonts.css">
  </head>
  <body>
  <%@ include file="/partials/navbar.jsp" %>
  <%@ include file="/partials/header.jsp" %>
  <br>
  <div class="text-center">
<span class="font-size-72 font-turretroad">JavaEE</span>
  </div>
  <br>
  <c:import url="/partials/footer.jsp">

  </c:import>
  </body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: student114
  Date: 10/20/20
  Time: 1:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSP Lesson</title>
    <%@include file="/partials/bootstrap.jsp"%>
    <link rel="stylesheet" href="/partials/googlefonts.css">
</head>
<body>
<%@ include file="/partials/navbar.jsp" %>
    <h3>JSP Expression</h3>
    <p>
        Convert a string to uppercase:
        <%= new String("I'm not yelling!").toUpperCase()%>
    </p>

    <p>
        25 times 4 is:
        <%= 25 * 4 %>
    </p>

    <p>
        is 75 less than 68?
        <%= 75<68%>
    </p>


    <h3>JSP Scriplet</h3>

    <%
        for (int i = 1; i <=5; i++){
            out.println("<br>"+ i);
        }
    %>

    <h3>JSP Declaration</h3>
<%-- create a method --%>

    <%!
       String makeItLowerCase(String slogan){
           return slogan.toLowerCase();
       }
    %>

<%--call a method--%>
    <%=
        makeItLowerCase("Melts in Your Mouth, Not in your Hands")
    %>


    <h3>Expression Language (EL)</h3>
    <%
        request.setAttribute("greeting","Hello, Servlet!");
    %>

    <p>Here is the message from our EL:
    ${greeting}
    </p>

<%--    variable needs to be set as an attribute to be used in EL--%>

</body>
</html>

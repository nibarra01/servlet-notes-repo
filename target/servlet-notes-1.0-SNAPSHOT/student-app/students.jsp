<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student114
  Date: 10/21/20
  Time: 10:24 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View Students</title>
    <%@include file="/partials/bootstrap.jsp"%>
    <link rel="stylesheet" href="/partials/googlefonts.css">
</head>
<body>
<%@ include file="/partials/navbar.jsp"%>
    <h1>Welcome</h1>
    <h3>Here are all the students:</h3>

    <table border="1">
        <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
        </tr>
<%--        JSTL directive--%>
        <c:forEach items="${listOfStudents}" var="enrolledStudents">
            <tr>
                <td>${enrolledStudents.firstName}</td>
                <td>${enrolledStudents.lastName}</td>
                <td>${enrolledStudents.email}</td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>

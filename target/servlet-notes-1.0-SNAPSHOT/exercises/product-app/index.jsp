<%--
  Created by IntelliJ IDEA.
  User: student114
  Date: 10/21/20
  Time: 11:42 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>index.html</title>
    <jsp:include page="/partials/bootstrap.jsp" />
    <link rel="stylesheet" href="/partials/googlefonts.css">
    <link rel="stylesheet" href="/exercises/product-app/common/style.css">
</head>

<body class="a">
<%@ include file="/partials/navbar.jsp"%>
<%@ include file="common/header.jsp"%>

<div class="row">
    <div class="col-12 text-center">
        <span class="font-coustard font-size-48">THIS IS THE INDEX</span>
    </div>
</div>


<div class="row text-center">
    <div class="col text-center">
        <a class="btn btn-info btn-lg btn-outline-warning font-coustard font-text-bold" href="../../app/list" role="button">Current WishCart</a>
    </div>
    <div class="col text-center ">
        <a class="btn btn-info btn-lg btn-outline-warning font-coustard font-text-bold" href="add.jsp" role="button">Consume product then get excited for next product</a>
    </div>
</div>

<%--<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>--%>
<%--<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>--%>
<%--<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>--%>

<br><br>
<%@ include file="common/footer.jsp"%>

</body>
</html>

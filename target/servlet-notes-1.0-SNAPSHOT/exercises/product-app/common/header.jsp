<%--
  Created by IntelliJ IDEA.
  User: nibarra
  Date: 10/24/2020
  Time: 12:13 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Header</title>
    <jsp:include page="/partials/bootstrap.jsp" />
    <link rel="stylesheet" href="/exercises/product-app/common/style.css">
    <link rel="stylesheet" href="/partials/googlefonts.css">
</head>
<body class>
<div class="text-center">
    <span class="font-size-24 font-coustard">Product-app Header</span>
</div>
</body>
</html>

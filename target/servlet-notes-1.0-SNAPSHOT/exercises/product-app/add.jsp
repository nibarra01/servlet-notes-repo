<%--
  Created by IntelliJ IDEA.
  User: student114
  Date: 10/21/20
  Time: 11:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Product</title>
    <jsp:include page="/partials/bootstrap.jsp" />
    <link rel="stylesheet" href="/partials/googlefonts.css">
    <link rel="stylesheet" href="/exercises/product-app/common/style.css">
</head>
<body class="a">
<%@ include file="/partials/navbar.jsp"%>
<%@ include file="common/header.jsp"%>

<div class="container">
    <div class="row text-center">
        <div class="col">
            <span class="font-coustard font-size-48">Enter the new product's info:</span>
        </div>
    </div>
    <form action="/app/add" method="post">
        <div class="row">
            <div class="col-2 text-right font-size-18 font-coustard">
                <label for="name">Name</label>
            </div>
            <div class="col-10">
                <input type="text" name="name" id="name" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col-2 text-right font-size-18 font-coustard">
                <label for="category">Category</label>
            </div>
            <div class="col-10">
                <input type="text" name="category" id="category" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col-2 text-right font-size-18 font-coustard">
                <label for="price">Price</label>
            </div>
            <div class="col-10">
                <input type="text" name="price" id="price" class="form-control">
            </div>
        </div>
        <div class="row">
            <div class="col text-center font-size-18 font-coustard">
                <input type="submit" value="<< Enter new product >>">
            </div>
        </div>
    </form>

    <div class="row text-center">
        <div class="col text-right">
            <a class="btn btn-info btn-outline-warning btn-lg font-coustard font-text-bold" href="index.jsp" role="button">Index</a>
        </div>
        <div class="col text-left">
            <a class="btn btn-info btn-outline-warning btn-lg font-coustard font-text-bold" href="../../app/list" role="button">Current WishCart</a>
        </div>
    </div>
</div>

<br><br>
<%@ include file="common/footer.jsp"%>

</body>
</html>

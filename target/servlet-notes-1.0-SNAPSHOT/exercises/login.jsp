<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student114
  Date: 10/20/20
  Time: 2:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>LOGIN</title>
    <jsp:include page="/partials/bootstrap.jsp" />
    <link rel="stylesheet" href="/partials/googlefonts.css">

</head>
<body>
<%@ include file="../partials/navbar.jsp" %>
<%@ include file="../partials/header.jsp" %>
<div class="container">
        <form method="post" action="/login" name="formy">
            <div class="form-group">
                <div class="row">
                    <div class="col-3 text-left">
                        <label for="username">
                            <span class="font-size-24 font-coustard">Enter username: </span>
                        </label>
                    </div>
                    <div class="col-9">
                        <input class="form-control" id="username" type="text" name="username">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-3 text-left">
                        <label for="password">
                            <span class="font-size-24 font-coustard">Enter password: </span>
                        </label>
                    </div>
                    <div class="col-9">
                        <input class="form-control font-coustard" id="password" type="password" name="password">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <div class="col text-center">
                            <input type="submit" name="button" value="Login" class="btn btn-danger btn-outline-light">
                        </div>
                    </div>
                </div>
            </div>
        </form>
</div>


<%--<c:choose>--%>
<%--    <c:when test="${param.username == 'admin' && param.password == 'password'}">--%>
<%--        <%--%>
<%--            response.sendRedirect("profile.jsp");--%>
<%--        %>--%>
<%--    </c:when>--%>
<%--    <c:when test="${param.username == 'stevejobs' && param.password == 'apple'}">--%>
<%--        <%--%>
<%--            response.sendRedirect("http://www.apple.com");--%>
<%--        %>--%>
<%--    </c:when>--%>
<%--    <c:when test="${param.username == 'billgates' && param.password == 'microsoft'}">--%>
<%--        <%--%>
<%--            response.sendRedirect("http://www.microsoft.com");--%>
<%--        %>--%>
<%--    </c:when>--%>
<%--</c:choose>--%>


<c:import url="../partials/footer.jsp">

</c:import>
</body>
</html>

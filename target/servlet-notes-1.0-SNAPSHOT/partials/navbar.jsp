<%--
  Created by IntelliJ IDEA.
  User: student114
  Date: 10/20/20
  Time: 3:41 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Navbar</title>
    <jsp:include page="/partials/bootstrap.jsp" />
    <link rel="stylesheet" href="/partials/googlefonts.css">
</head>
<body>
<%--<hr>--%>
<%--<a href="../exercises/login.jsp">Login</a>&nbsp;&nbsp;<a href="../jsp-lesson.jsp">JSP lesson</a>&nbsp&nbsp <a href="../jstl-lesson.jsp">JSTL lesson</a>--%>
<%--<a href="../count">Counter</a> <a href="../ping">Ping</a>--%>
<%--<hr>--%>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="/">Home</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="/login">Login</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle active" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Servlet Exercises
                </a>
                <div class="dropdown-menu active" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/hello-world">Hello World</a>
                    <a class="dropdown-item" href="/count">Count</a>
                    <a class="dropdown-item" href="/ping">Ping-pong</a>
                </div>
            </li>
            <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    MVC Lesson
                </a>
                <div class="dropdown-menu active" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/students">Part 1 - list</a>
                    <a class="dropdown-item" href="/students/add-student">Part 2 - add</a>
                </div>
            </li>
            <li class="nav-item dropdown active">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Cookies-Session Lesson
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <a class="dropdown-item" href="/admin-login">Login</a>
                    <a class="dropdown-item" href="/admin-profile">Profile</a>
                    <a class="dropdown-item" href="/admin-logout">Logout</a>
                </div>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/exercises/product-app/index.jsp">Wishlist app</a>
            </li>
        </ul>
    </div>
</nav>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>

</body>
</html>

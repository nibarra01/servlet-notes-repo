<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student114
  Date: 10/20/20
  Time: 1:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>JSTL Lesson</title>
    <%@include file="/partials/bootstrap.jsp"%>
    <link rel="stylesheet" href="/partials/googlefonts.css">
</head>
<body>
<%@ include file="/partials/navbar.jsp" %>
    <h1>JSTL Directives</h1>

    <h3>choose, when, otherwise</h3>

<%--example--%>
    <c:set var="num" value="12">

    </c:set>


    <c:choose>
        <c:when test="${num % 2 == 0}">
            <c:out value="${num} is an even number">

            </c:out>
        </c:when>

        <c:otherwise>
            <c:out value="${num} is an odd number">

            </c:out>
        </c:otherwise>
    </c:choose>

    <h3>c:forEach</h3>

    <c:forEach var="x" begin="1" end="5">
        Item <c:out value="${x}" /><br>
    </c:forEach>

    <%
        request.setAttribute("digits",new int[]{11,12,13,14,15});
    %>

    <ul>
        <c:forEach items="${digits}" var="myDigits">
            <li>${myDigits}</li>
        </c:forEach>
    </ul>

<%--    if tag--%>
    <c:if test="${isAdmin}">
        <h1>Welcome back Admin!</h1>
        <%--        extra content, only viewable by Admin... --%>
    </c:if>
    <c:choose>
        <c:when test="${cart.isEmpty()}">
            <h2>No items in your cart (yet).</h2>
        </c:when>
        <c:otherwise>
            <c:forEach var="item" items="${cart.items}">
                <div class="item">
                    <h3>${item.name}</h3>
                    <p>${item.description}</p>
                    <p>${item.price}</p>
                        <%-- using the c:if tag to display a message if the item is on sale --%>
                    <c:if test="${item.isOnSale}">
                        <p>This item is on sale!</p>
                    </c:if>
                </div>
            </c:forEach>
        </c:otherwise>
    </c:choose>
</body>
</html>

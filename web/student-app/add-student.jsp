<%--
  Created by IntelliJ IDEA.
  User: student114
  Date: 10/21/20
  Time: 10:55 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add a student</title>
    <%@include file="/partials/bootstrap.jsp"%>
    <link rel="stylesheet" href="/partials/googlefonts.css">
</head>
<body>
<%@ include file="/partials/navbar.jsp"%>
    <h3>Enter the new student's info:</h3>
    <form action="/students/add-student" method="post">
        <label for="firstName">First Name</label>
        <input type="text" name="firstName" id="firstName">
        <br>
        <label for="lastName">Last Name</label>
        <input type="text" name="lastName" id="lastName">
        <br>
        <label for="email">Email</label>
        <input type="text" name="email" id="email">
        <br>
        <input type="submit" value="<< Enter new student >>">
    </form>
</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%--
  Created by IntelliJ IDEA.
  User: student114
  Date: 10/21/20
  Time: 11:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product List</title>
    <jsp:include page="/partials/bootstrap.jsp" />
    <link rel="stylesheet" href="/exercises/product-app/common/style.css">
    <link rel="stylesheet" href="/partials/googlefonts.css">
</head>
<body class="a">
<%@ include file="/partials/navbar.jsp"%>
<%@ include file="common/header.jsp"%>


<div class="text-center"><span class="text1">Current List of Products</span></div>
<div class="container">
    <div class="row borderA text-center">
        <c:forEach items="${P}" var="pl" varStatus="loop">
            <c:choose>
                <c:when test="${loop.first}">
                    <div class="col-4 borderB-first"><span class="text3">NAME:</span></div>
                    <div class="col-4 borderC-first"><span class="text3">PRICE:</span></div>
                    <div class="col-4 borderD-first"><span class="text3">CATEGORY:</span></div>

                    <div class="col-4 borderB"><span class="text2"></span> ${pl.name}</div>
                    <div class="col-4 borderC"><span class="text2"></span><fmt:setLocale value="en_US" /> <fmt:formatNumber type="currency">${pl.price}</fmt:formatNumber></div>
                    <div class="col-4 borderD"><span class="text2"></span> ${pl.category}</div>
                </c:when>
                <c:when test="${!loop.first}">
                    <div class="col-4 borderB"><span class="text2"></span> ${pl.name}</div>
                    <div class="col-4 borderC"><span class="text2"></span> <fmt:setLocale value="en_US" /> <fmt:formatNumber type="currency">${pl.price}</fmt:formatNumber></div>
                    <div class="col-4 borderD"><span class="text2"></span> ${pl.category}</div>
                </c:when>
            </c:choose>
        </c:forEach>
    </div>
    <br>
    <div class="row text-center">
        <div class="col text-right">
            <a class="btn btn-info btn-outline-warning btn-lg font-coustard font-text-bold" href="/exercises/product-app/index.jsp" role="button">Index</a>
        </div>
        <div class="col text-left">
            <a class="btn btn-info btn-outline-warning btn-lg font-coustard font-text-bold" href="/exercises/product-app/add.jsp" role="button">Add</a>
        </div>
    </div>
</div>

<br><br>
<%@ include file="common/footer.jsp"%>

</body>
</html>

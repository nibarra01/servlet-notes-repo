<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: student114
  Date: 10/20/20
  Time: 2:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>profile.jsp</title>
    <jsp:include page="/partials/bootstrap.jsp" />
    <link rel="stylesheet" href="/partials/googlefonts.css">
    <link rel="stylesheet" href="profile/style.css">
</head>
<body>
<%@ include file="../partials/navbar.jsp" %>
<%@ include file="profile/header.jsp" %>


<span class="font-redhatdisplay font-size-48 font-color-red">Some more stuff here later</span>


<div class="text-center">
    <a href="/logout" role="button" class="btn btn-sm btn-success font-redhatdisplay">Logout</a>
</div>




<c:import url="profile/footer.jsp">

</c:import>
<script src="profile/js.js"></script>
</body>
</html>

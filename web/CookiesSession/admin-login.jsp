<%--
  Created by IntelliJ IDEA.
  User: student114
  Date: 10/22/20
  Time: 8:44 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Sessions & Cookies Login</title>
    <jsp:include page="/partials/bootstrap.jsp" />
</head>
<body>
<%@ include file="/partials/navbar.jsp"%>
<%@ include file="header.jsp"%>

<div class="container">
    <div class="row text-center">

        <div class="col font-aldrich font-size-24">
            <form action="/admin-login" method="post">
                <div class="form-group">
                    <input type="text" id="username" name="username" placeholder="Username">
                </div>
                <div class="form-group">
                    <input type="password" id="password" name="password" placeholder="Password">
                </div>
                <span>
                    <input type="submit" value="Login">
                </span>
            </form>
        </div>
    </div>
</div>

<%@ include file="footer.jsp"%>

</body>
</html>

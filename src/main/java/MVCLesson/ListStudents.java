package MVCLesson;

import java.util.ArrayList;
import java.util.List;

//DAO implementation
public class ListStudents implements Students{
//    incorporate our DAO
    private List<Student> students = new ArrayList<>();
//    created an empty list of students

//    note - when an instance of this class is created, will populate students' info

    public ListStudents(){
        insert(new Student("Leslie", "Knope", "leslie@pawnee.com"));
        insert(new Student("Ron", "Swanson", "ron@pawnee.com"));
        insert(new Student("Ben", "Wyatt", "ben@pawnee.com"));
    }
    @Override
    public List<Student> all() {
        return this.students;
    }

    @Override
    public void insert(Student student) {
        this.students.add(student);
    }
}

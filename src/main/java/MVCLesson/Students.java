package MVCLesson;

import java.util.List;

//DAO interface
public interface Students {
    List<Student> all();

//     add a new student
    void insert(Student student);

}

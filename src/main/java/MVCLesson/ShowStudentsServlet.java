package MVCLesson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ShowStudentsServlet", urlPatterns = "/students")
public class ShowStudentsServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        incorporate DAO
//        use the factory to get the DAO object
        Students studentsDao = DAOFactory.getStudentsDao();

//        use the method on the DAO to get all of the students
        List<Student> students = studentsDao.all();

//        pass the data to the jsp file (View)
        request.setAttribute("listOfStudents", students);
        request.getRequestDispatcher("/student-app/students.jsp").forward(request,response);
    }
}

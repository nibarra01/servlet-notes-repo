package MVCLesson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "AddStudentServlet", urlPatterns = "/students/add-student")
public class AddStudentServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        bring in getStudentsDao()
        Students studentsDao = DAOFactory.getStudentsDao();
//        enroll a new student based on the submitted data
        String newFirstName = request.getParameter("firstName");
        String newLastName = request.getParameter("lastName");
        String newEmail = request.getParameter("email");

        Student newStudent = new Student(newFirstName, newLastName, newEmail);

//        add the new student by using the insert() from interface
        studentsDao.insert(newStudent);

//        send the user to the /students
        response.sendRedirect("/students");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        where is the data coming from
        request.getRequestDispatcher("/student-app/add-student.jsp").forward(request,response);
    }
}

package CodeBoundWishList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "ServletList", urlPatterns = "/app/list")
public class ServletList extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        DAO
        ProductInterface theDAO = daof.getProductDAO();

//        get list from DAO
        List<Product> pList = theDAO.all();

//        send data to display
        request.setAttribute("P",pList);
        request.getRequestDispatcher("/exercises/product-app/list.jsp").forward(request,response);
    }
}

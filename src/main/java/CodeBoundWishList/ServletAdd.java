package CodeBoundWishList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletAdd", urlPatterns = "/app/add")
public class ServletAdd extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        DAO
        ProductInterface DAO = daof.getProductDAO();

//        build new
        String newName = request.getParameter("name");
        double newPrice = Double.parseDouble(request.getParameter("price"));
        String newCatagory = request.getParameter("category");

        Product newProduct = new Product(newName, newCatagory, newPrice);

//        add new
        DAO.insert(newProduct);

//        send new
        response.sendRedirect("/app/list");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        receive parts to build new
        request.getRequestDispatcher("/exercises/product-app/add.jsp").forward(request, response);
    }
}

package CodeBoundWishList;

import java.util.ArrayList;
import java.util.List;

public class ListProducts implements ProductInterface{
    private List<Product> products = new ArrayList<>();

    public ListProducts() {
        insert(new Product("Test 1", "Test Category A", 99.99));
        insert(new Product("Test 2", "Test Category A", 5));
        insert(new Product("Test 3", "Test Category B", 45.2));
    }

    @Override
    public List<Product> all() {
        return this.products;
    }

    @Override
    public void insert(Product addition) {
        this.products.add(addition);
    }
}

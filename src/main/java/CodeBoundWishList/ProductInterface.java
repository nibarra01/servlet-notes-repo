package CodeBoundWishList;

import java.util.List;

public interface ProductInterface {
    List<Product> all();

    void insert(Product addition);
}

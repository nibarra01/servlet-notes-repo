import javax.persistence.criteria.CriteriaBuilder;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "ServletLOGIN", urlPatterns = "/login")
public class ServletLOGIN extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String[][] list = usernames.getUsers();

        int id = -128;

        for (String[] users : list) {
            if (users[0].equals(username) && users[1].equals(password)) {
                id = Integer.parseInt(users[2]);
            }
        }

        switch (id ){
            case 0:
                request.getSession().setAttribute("loggedIn", true);
                request.getSession().setAttribute("username",username);
                response.sendRedirect("/admin");
                break;
            case 1:
                request.getSession().invalidate();
                response.sendRedirect("http://www.apple.com");
                break;
            case 2:
                request.getSession().invalidate();
                response.sendRedirect("http://www.microsoft.com");
                break;
            case 3:
                response.sendRedirect("/count");
                break;
            case 4:
                response.sendRedirect("/students");
                break;
            default:
            response.sendRedirect("/login");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession theSession = request.getSession();
        boolean a = false;

        if (theSession.getAttribute("loggedIn") != null){
            a = (boolean) theSession.getAttribute("loggedIn");
        }

        if (a){
            request.getRequestDispatcher("/exercises/profile.jsp").forward(request,response);

        } else {
            request.getRequestDispatcher("/exercises/login.jsp").forward(request,response);
        }
    }
}

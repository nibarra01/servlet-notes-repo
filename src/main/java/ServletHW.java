import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletHW", urlPatterns = "/hello-world")
public class ServletHW extends javax.servlet.http.HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();

//        writer.println("<h1><span style=\"color:blue\">Hello World</span></h1>");
//        String test = request.getRequestURI();
//        String test = request.getRequestURL();
        String test = request.getQueryString();
//        writer.println("<p>" + test + "</p>");

        if (test == null || !test.contains("name=")){
                    writer.println("<h1><span style=\"color:blue\">Hello World</span></h1>");
                    writer.println("<a href=\"/hello-world?name=person\"><h3><span style='color:green'>/hello-world?name=person</span></h3></a>");
        } else  {
            int cut = test.indexOf("=");
            writer.println("<h1><span style=\"color:blue\">Hello " + test.substring(cut+1) + "</span></h1>");
        }
        writer.println("<br><br><br><br><a href=\"/\">Return</a>");
    }
}

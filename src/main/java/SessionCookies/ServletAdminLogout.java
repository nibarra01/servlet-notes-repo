package SessionCookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ServletAdminLogout", urlPatterns = "/admin-logout")
public class ServletAdminLogout extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        destroy the session object
        request.getSession().invalidate();

//        redirect user back to adnim-login.jsp
        response.sendRedirect("/admin-login");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        copy paste from doPost so the redirect happens

//        destroy the session object
        request.getSession().invalidate();

//        redirect user back to adnim-login.jsp
        response.sendRedirect("/admin-login");
    }
}

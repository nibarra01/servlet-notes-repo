package SessionCookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "ServletAdminLogin", urlPatterns = "/admin-login")
public class ServletAdminLogin extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        storing information in our session and use that to restrict access to different parts of our app

//        variable assigned to the parameters from our login view
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        boolean validAttemnt = username.equals("admin") && password.equals("password123");

        if(validAttemnt){
            request.getSession().setAttribute("user",true);
//            sets an attribute as "user" to 'true' if the login attempt was valid
            request.getSession().setAttribute("username",username);
//            sets an attribute "username" to the value of the above String variable 'username'
            response.sendRedirect("/admin-profile");
//            sends user to the admin profile
        } else {
            response.sendRedirect("/admin-login");
//        invalid attempt
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        boolean isUser = false;

        if (session.getAttribute("user") != null){
            isUser = (boolean) session.getAttribute("user");
//            session.getAttribute - used when we want to retrieve information from the session
        }

        if (isUser){
            request.getRequestDispatcher("/CookiesSession/admin-profile.jsp").forward(request,response);
//            if true, sends user to admin-profile.jsp view
        } else {
            request.getRequestDispatcher("/CookiesSession/admin-login.jsp").forward(request,response);
        }
    }
}

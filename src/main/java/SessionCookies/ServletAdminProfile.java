package SessionCookies;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "ServletAdminProfile", urlPatterns = "/admin-profile")
public class ServletAdminProfile extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        boolean isUser = false;

        if (session.getAttribute("user") != null){
            isUser = (boolean) session.getAttribute("user");
        }

        if (isUser){
            request.setAttribute("username", session.getAttribute("username"));
//            get the attribute set in the login servlet
            request.getRequestDispatcher("CookiesSession/admin-profile.jsp").forward(request,response);
        } else {
            request.getRequestDispatcher("/CookiesSession/admin-login.jsp").forward(request,response);
        }
    }
}

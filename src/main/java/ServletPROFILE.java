import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "ServletPROFILE", urlPatterns = "/admin")
public class ServletPROFILE extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        if I understand the notes correctly I can just paste this here?
        HttpSession theSession = request.getSession();
        boolean a = false;

        if (theSession.getAttribute("loggedIn") != null){
            a = (boolean) theSession.getAttribute("loggedIn");
        }

        if (a){
            request.getRequestDispatcher("/exercises/profile.jsp").forward(request,response);
        } else {
            request.getRequestDispatcher("/exercises/login.jsp").forward(request,response);
        }
    }
}

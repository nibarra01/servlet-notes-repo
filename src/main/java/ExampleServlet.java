import javax.servlet.annotation.WebServlet;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ExampleServlet", urlPatterns = "/example")
public class ExampleServlet extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
//        1 set the content type
        response.setContentType("text/html");

//        2 get the PrintWriter
        PrintWriter writer = response.getWriter();

//        3 generate HTML content
        writer.println("<h1>Example Servlet</h1>");
        writer.println("<p>Created by Charlie Cohort</p>");

    }
}

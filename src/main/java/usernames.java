import java.util.HashMap;

public interface usernames {
    String[][] validUsers = new String[][]{
            {"admin", "password", "0"},
            {"stevejobs", "apple", "1"},
            {"billgates", "microsoft", "2"},
            {"counting", "countdown321", "3"},
            {"principal", "wedontneednoeducation", "4"}
    };

    static String[][] getUsers(){
        return validUsers;
    }

}

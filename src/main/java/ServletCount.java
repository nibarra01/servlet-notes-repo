import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "ServletCount", urlPatterns = "/count")
public class ServletCount extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        counter++;
        response.setContentType("text/html");
        PrintWriter writer = response.getWriter();
        writer.println("<h2><span class=\"blink\">YOU ARE VISITOR #" + counter + "!!!</span></h2>");
        writer.println("<style> .blink { animation: blinker 1.25s linear infinite; color: #10f010; font-size: 36px; } @keyframes blinker { 50% { opacity: 0; )}</style>");
        writer.println("<h4><a href=\"/count\">Refresh</a></h4>");
        writer.println("<br><br><br><br><a href=\"/\">Return</a>");

    }
    private int counter = 0;
}
